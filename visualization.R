hr <- read.csv("HR-Employee-Attrition.csv")
x<- hr$EducationField
x
y <-hr$MonthlyIncome
y
pdf("visualization.pdf")
colour <- c(rep("Cyan",1),rep("yellow",1),rep("Green",1),rep("orange",1), rep("Grey",1),rep("Violet",1))
boxplot(y~x, main="Education Field vs Monthly Income", 
        xlab="Education Field", ylab="Monthly Income",
        frame=T,col=colour, cex.axis=0.5, ylim=c(0,20000))
h <-hist(hr$MonthlyIncome,20,main="Histogram of Monthly Income",
         xlab = "Monthly Income",col="Cyan")
mn <- mean(y)
mn
stdD <- sd(y)
stdD
x1 <- seq(0,20000,1)
y1 <- dnorm(x1,mean=mn,sd=stdD)
y1 <-y1*diff(h$mids[1:2]) * length(y);
lines(x1,y1,col="Red")

#dev.off()
